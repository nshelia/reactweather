var React = require('react');

var WeatherMessage = ({temp, location}) => {
	return (
		<h3 className="text-center">Weather in {location} - ({temp}) </h3>
	);
}

module.exports = WeatherMessage;